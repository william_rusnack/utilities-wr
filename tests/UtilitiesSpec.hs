module UtilitiesSpec where

import Utilities

import Test.Hspec

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  it "bimap'" $ do
    bimap' (* 2) (1, 3) `shouldBe` ((2, 6) :: (Int, Int))

  it "nand" $ do
    nand True True `shouldBe` False
    nand True False `shouldBe` True
    nand False True `shouldBe` True
    nand False False `shouldBe` True

