{-# LANGUAGE ScopedTypeVariables
           , ViewPatterns
           , TypeApplications
           , PartialTypeSignatures #-}

module Utilities.ArbitrarySpec where

import Utilities.Arbitrary

import Control.Applicative (liftA2)
import Data.Tuple (swap)
import Test.Hspec
import Test.QuickCheck

import Prelude hiding (head, length, reverse, zip)


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  it "minChoose within bounds" $ do
    property $ \(NonNegative s) -> let minB = maxGenValue - s in
      forAll (minChoose minB) $ \v -> v >= minB && v <= maxGenValue

  it "minChooseD within bounds" $ do
    property $ \(NonNegative s) (Positive divisor) ->
      let maxB = maxGenValue `div` divisor
          minB = maxB - s
      in forAll (minChooseD divisor minB) $ \v ->
        v >= minB && v <= maxB

  describe "AddNoise Double" $ do
    describe "arNoise" $ do
      describe "absolute" $ do
        it "within bounds" $ do
          let test a v = forAll (arNoise a 0 (v :: Double)) $
                         \nv -> nv >= minB && nv <= maxB
                where minB = v - (abs a)
                      maxB = v + (abs a)
          property test

        it "noise added" $ do
          property $ \(NonZero a) v -> do
            ns <- sample' (arNoise a 0 v :: Gen Double)
            any (/= v) ns `shouldBe` True

      describe "relative" $ do
        it "within bounds" $ do
          let test r v = forAll (arNoise 0 r (v :: Double)) $
                         \nv -> nv >= minB && nv <= maxB
                where minB = v - dr
                      maxB = v + dr
                      dr = abs $ r * v
          property test

        it "noise added" $ do
          property $ \(NonZero r) (NonZero v) -> do
            ns <- sample' (arNoise 0 r v :: Gen Double)
            any (/= v) ns `shouldBe` True

    describe "arNoiseUnbounded" $ do
      describe "absolute" $ do
        it "out of bounds" $ do
          property $ \a v -> forAll (arNoiseUnbounded a 0 (v :: Double)) $
                             \nv -> abs (nv - v) > abs a

      describe "relative" $ do
        it "out of bounds" $ do
          property $ \r v -> forAll (arNoiseUnbounded 0 r (v :: Double)) $
                     \nv -> abs ((nv - v) / v) > abs r

  describe "choose" $ do
    let tupleSort vs@(x, y) = if x <= y then vs else swap vs

    it "chooseXL" $ property $
      (\bs@(l, u) -> forAll (uncurry chooseXL bs :: Gen Int) $ \v -> v > l && v <= u) .
      tupleSort .
      getNonEqual

    it "chooseXU" $ property $
      (\bs@(l, u) -> forAll (uncurry chooseXU bs :: Gen Int) $ \v -> v >= l && v < u) .
      tupleSort .
      getNonEqual

    it "chooseXLU" $ property $
      (\bs@(l, u) -> forAll (uncurry chooseXLU bs :: Gen Double) $ \v -> v > l && v < u) .
      tupleSort .
      getNonEqual

  it "lessThan" $ property $
    liftA2 forAll
      (lessThan @Int)
      (\x v -> v < x)

  it "lessThanOrEqualTo" $ property $
    liftA2 forAll
      (lessThanOrEqualTo @Int)
      (\x v -> v <= x)

  it "greaterThan" $ property $
    liftA2 forAll
      (greaterThan @Int)
      (\x v -> v > x)

  it "greaterOrEqualTo" $ property $
    liftA2 forAll
      (greaterOrEqualTo @Int)
      (\x v -> v >= x)

