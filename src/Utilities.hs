{-# LANGUAGE ScopedTypeVariables
           , ViewPatterns #-}

module Utilities where

import Control.Monad ((<=<), return)
import Data.Bifunctor (Bifunctor(bimap))
import Data.Bitraversable (Bitraversable(bitraverse))


bimap' :: Bifunctor p => (a -> b) -> p a a -> p b b
bimap' f = bimap f f

t3to2 :: (a, b, c) -> (a, b)
t3to2 (x, y, _) = (x, y)

t4to2 :: (a, b, c, d) -> (a, b)
t4to2 = t3to2 . t4to3

t4to3 :: (a, b, c, d) -> (a, b, c)
t4to3 (x, y, z, _) = (x, y, z)

mkFst :: (a -> b) -> a -> (b, a)
mkFst f a = (f a, a)

mkSnd :: (a -> b) -> a -> (a, b)
mkSnd f a = (a, f a)

mkThd :: (a -> b -> c) -> (a, b) -> (a, b, c)
mkThd f (x, y) = (x, y, f x y)

-- Source: https://www.stackage.org/haddock/lts-12.5/Agda-2.5.4.1/src/Agda.Utils.Maybe.html#unzipMaybe
unzipMaybe :: Maybe (a,b) -> (Maybe a, Maybe b)
unzipMaybe Nothing      = (Nothing, Nothing)
unzipMaybe (Just (a,b)) = (Just a, Just b)

maybeBool :: Maybe Bool -> Bool
maybeBool (Just b) = b
maybeBool Nothing = False

class Zero a where
  zero :: a
instance Zero Int where zero = 0
instance Zero Double where zero = 0

infixl 1 <&>
(<&>) :: Functor f => f a -> (a -> b) -> f b
(<&>) = flip fmap

infixr 1 <$<
(<$<) :: Monad m => (b -> c) -> (a -> m b) -> a -> m c
f0 <$< f1 = return . f0 <=< f1

nor :: Bool -> Bool -> Bool
nor = curry (not . uncurry (||))

nand :: Bool -> Bool -> Bool
nand = curry $ not . uncurry (&&)

iff :: (a -> Bool) -> (a -> b) -> (a -> b) -> a -> b
iff p f0 f1 v = (if p v then f0 else f1) v

ifv :: (a -> Bool) -> b -> b -> a -> b
ifv p v0 v1 v = if p v then v0 else v1

biLift :: forall a b c d e. (a -> b -> c) -> (d -> a) -> (e -> b) -> d -> e -> c
biLift f f0 f1 x y = f (f0 x) (f1 y)
biLift' :: forall a b c d e. (a -> b -> c) -> (d -> a) -> (e -> b) -> (d, e) -> c
biLift' f f0 f1 = uncurry $ biLift f f0 f1

biLiftC :: forall a b c. (a -> a -> b) -> (c -> a) -> c -> c -> b
biLiftC f f0 = biLift f f0 f0
biLiftC' :: forall a b c. (a -> a -> b) -> (c -> a) -> (c, c) -> b
biLiftC' f f0 = uncurry $ biLiftC f f0

bitraverse' :: forall f a t b. (Applicative f, Bitraversable t)
            => (a -> f b) -> t a a -> f (t b b)
bitraverse' f = bitraverse f f
