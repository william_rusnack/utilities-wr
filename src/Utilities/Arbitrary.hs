{-# LANGUAGE GeneralizedNewtypeDeriving
           , ViewPatterns
           , PartialTypeSignatures
           , ScopedTypeVariables
           , UndecidableInstances #-}

module Utilities.Arbitrary where

import Control.Applicative (liftA2)
import Control.Monad
import System.Random (Random)

import Test.QuickCheck

import Prelude hiding (head, replicate)


maxGenValue :: Int
maxGenValue = 15

minChoose :: Int -> Gen Int
minChoose = minChooseD 1

minChooseD :: Int -> Int -> Gen Int
minChooseD divisor minV = choose (minV, maxGenValue `div` divisor)

newtype NonEqual a = NonEqual {getNonEqual :: (a, a)} deriving Show
instance (Arbitrary a, Eq a) => Arbitrary (NonEqual a) where
  arbitrary = do
    x <- arbitrary
    y <- suchThat arbitrary (/= x)
    return $ NonEqual (x, y)

class AddNoise a where
  -- absolute relative noise
  -- max bound dominates
  arNoise :: Double -- +/- absolute/fixed value of noise
          -> Double -- ratio of the value to it's noise
          -> a
          -> Gen a

  -- max bound dominamtes
  arNoiseUnbounded :: Double -- +/- absolute/fixed value of noise
                   -> Double -- ratio of the value to it's noise
                   -> a
                   -> Gen a

instance AddNoise Double where
  arNoise a r v = do
    let b = max (abs a) (abs $ r * v)
    choose (v - b, v + b)

  arNoiseUnbounded a r v = do
    let b = max (abs a) (abs $ r * v)
    n <- elements [id, negate] <*> ((* b) . (+ 1) . abs . getNonZero <$> arbitrary)

    if n /= 0
      then return $ v + n
      else suchThat arbitrary (/= v)

-- https://github.com/nick8325/quickcheck/pull/247
cchoose :: Random a => a -> a -> Gen a
cchoose = curry choose

chooseXL :: (Eq a, Random a, Show a) => a -> a -> Gen a
chooseXL l u | l /= u     = suchThat (cchoose l u) (/= l)
             | otherwise  = error $ "l and u cannot both be: " ++ show l

chooseXU :: (Eq a, Random a, Show a) => a -> a -> Gen a
chooseXU l u | l /= u     = suchThat (cchoose l u) (/= u)
             | otherwise  = error $ "l and u cannot both be: " ++ show l

chooseXLU :: (Eq a, Random a, Show a) => a -> a -> Gen a
chooseXLU l u | l /= u    = suchThat (cchoose l u) (liftA2 (&&) (/= l) (/= u))
              | otherwise = error $ "l and u cannot both be: " ++ show l

lessThan :: (Ord a, Arbitrary a, Num a) => a -> Gen a
lessThan x = (-) x . getPositive <$> arbitrary

lessThanOrEqualTo :: (Ord a, Arbitrary a, Num a) => a -> Gen a
lessThanOrEqualTo x = (+) x . getNonPositive <$> arbitrary

greaterThan :: (Ord a, Arbitrary a, Num a) => a -> Gen a
greaterThan x = (+ x) . getPositive <$> arbitrary

greaterOrEqualTo :: (Ord a, Arbitrary a, Num a) => a -> Gen a
greaterOrEqualTo x = (+ x) . getNonNegative <$> arbitrary

thisOrThat :: a -> a -> Gen a
thisOrThat x y = elements [x, y]

